#include "Ackerman.h"
#include "BuddyAllocator.h"
#include <cstdlib>
#include <stdio.h>
#include <unistd.h>
#include <ctype.h>
int main(int argc, char **argv)
{

    int c;
    unsigned int basicBlockSize=128;
    unsigned int memorySize= 512 * 1024 ;

    if (argc<3)

        while ((c = getopt(argc, argv, "b:s:")) != -1) {
            switch (c) {
                case 'b':
                    basicBlockSize = atoi(optarg);
                    break;
                case 's':
                    memorySize = atoi(optarg);
                    break;
                case '?':
                    if (optopt == 'c')
                        fprintf(stderr, "Option -%c requires an argument.\n", optopt);
                    else if (isprint(optopt)) {
                        fprintf(stderr, "Unknown option `-%c'.\n", optopt);
                    } else {
                        fprintf(stderr, "Unknown option character `\\x%x'.\n", optopt);
                    }
                    return 1;
                default:
                    abort();
            }
        }
    Ackerman* ackerman = new Ackerman ();


    BuddyAllocator* buddyAllocator = new BuddyAllocator(basicBlockSize,memorySize);
    ackerman->test(buddyAllocator);
    //test_list();

    delete buddyAllocator;

    return 0;
}
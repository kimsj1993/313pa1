/*
    File: my_allocator.cpp
*/
#include "BuddyAllocator.h"
#include <iostream>
#include <vector>
#include <math.h>

using namespace std;
int i = 0;
int j = 0;

uint round_up(int v)
{
    //get this one from stackover flow
    v--;
    v |= v >> 1;
    v |= v >> 2;
    v |= v >> 4;
    v |= v >> 8;
    v |= v >> 16;
    v++;

    return v;
}
int BuddyAllocator::getPosition(uint length)
{
    uint i = basic_block_size;
    uint position = 0;

    for(;;)
    {
        if(i >= length)
            break;
        i = i << 1;
        position++;
    }
    return position;
}

void LinkedList::insert(BlockHeader *b)
{
    this->length ++;
    b->free = true;

    if(tail)
    {
        tail->next = b;
        b->pre = tail;
        tail = b;
    }
    else {
        head = tail = b;
    }

}

void LinkedList::remove(BlockHeader *b)
{
    if(b->pre)

        b->pre->next = b->next;

    if(b->next)
        b->next->pre = b->pre;

    if(b == head)
        head = b->next;
    if(b == tail)
        tail = b->pre;

    b->free = false;
    length -- ;


}

BlockHeader* LinkedList::removeTail()
{
    BlockHeader* block;

    if(head)
    {
        if(head == tail)
        {
            block = head;
            head = tail = NULL;
            length = 0;
        } else {
            block = tail;
            tail->pre->next = NULL;
            tail = tail->pre;
            length -= 1;
        }
    }

    block->free = false;
    block->pre = NULL;
    block->next = NULL;

    return block;
}



BuddyAllocator::BuddyAllocator (int _basic_block_size, int _total_memory_length){


    basic_block_size = round_up(_basic_block_size);
    total_memory_length = _total_memory_length;

    max_position = getPosition(_total_memory_length);

    memory_ptr = new char[total_memory_length];
    space = total_memory_length;

    BlockHeader* block_header = (BlockHeader*)memory_ptr;
    block_header->size = total_memory_length;
    block_header->next = NULL;
    block_header->pre = NULL;
    block_header->position = max_position;


    int temp = basic_block_size;
    while(temp <= total_memory_length)
    {
        LinkedList linked_list;
        free_list.push_back(linked_list);
        temp *= 2;
    }
    // max position
    free_list[max_position].insert(block_header);

}

BuddyAllocator::~BuddyAllocator (){
    delete memory_ptr;
}

char* BuddyAllocator::alloc(int _length) {

    cout << "in Alloc : "  << ++j  << endl;

    if(j == 75)
    {

    }

    /* This preliminary implementation simply hands the call over the
       the C standard library!
       Of course this needs to be replaced by your implementation.
    */


    int length = round_up(_length);
    while(length < basic_block_size)
    {
        length = length<< 1;
    }
    int expected_position = getPosition(length);

    if(space < length)
        return 0;

    space = space - length;


    uint i = 0;
    int count;
    BlockHeader *block;
    BlockHeader *block1;

    for(i = expected_position; i<= max_position; i++)
    {
        count = free_list[i].getLength();

        if(count > 0) break;
    }

    block = free_list[i].removeTail();

    while(i > expected_position)
    {
        block1 = (BlockHeader*) split(block);
        free_list[block1->position].insert(block1);
        i --;
    }

    if(i == expected_position)
    {
        return (char*)block + sizeof(BlockHeader);
    } else {
        return 0;
    }

    /*
    if(free_list[expected_position].tail)
    {
        free_list[expected_position].remove(free_list[expected_position].tail);
        return (char*)tempBlock + sizeof(BlockHeader);
    } else {
        while (expected_position <= max_position)
        {
            if(free_list[expected_position].tail)
            {
                tempBlock = free_list[expected_position].removeTail();
                while(1)
                {
                    if(tempBlock->size <= length)
                        break;
                    tempBlock = split(tempBlock);
                    free_list[expected_position].insert(tempBlock);
                }
                return (char*)tempBlock + sizeof(BlockHeader);
            }
            expected_position++;
        }
        return (char*)0;
    }
     */
}

BlockHeader* BuddyAllocator::split(BlockHeader *block)
{
    BlockHeader* left = (BlockHeader*) block;
    left->position -=1;
    left-> size = left->size /2;

    BlockHeader* right = (BlockHeader*) ((char*)block + left->size);
    right->free = true;
    right->next = NULL;
    right->pre = NULL;
    right->position = left->position;
    right->size = left->size;

    return right;

}

BlockHeader* BuddyAllocator::getbuddy(BlockHeader *addr)
{
    BlockHeader* blockHeader =(BlockHeader*) addr;
    BlockHeader* buddy = (BlockHeader*)((((char*)addr- memory_ptr) ^ blockHeader->size) + memory_ptr);

    return buddy;
}


bool BuddyAllocator::arebuddies(BlockHeader *block1, BlockHeader *block2)
{
    return getbuddy(block1) == block2 ? true : false;

}

BlockHeader* BuddyAllocator::merge(BlockHeader *block1, BlockHeader *block2)
{
    BlockHeader* block;
    if(block1 < block2)
    {
        block = block1;

    } else {
        block = block2;
    }
    block->position += 1;
    block->size *=2;
    return  block;

}

int BuddyAllocator::free(char* _a) {
    /* Same here! */
    cout << "in FREE : "  << ++i  << endl;
    if(i==43)
    {

    }
    BlockHeader* block = (BlockHeader*)(_a - sizeof(BlockHeader));

    while(true)
    {
        BlockHeader* buddy = getbuddy(block);

        if(block->position != max_position || (block->position != buddy->position) || !buddy->free)
            break;

        free_list[buddy->position].remove(buddy);

        if(arebuddies(block,buddy))
        {
            block = merge(block,buddy);
        }
    }
    free_list[block->position].insert(block);
    space += block->size;

    return 0;
}

void BuddyAllocator::debug (){
    for(int i = 0; i<free_list.size(); ++i)
    {
        int j= 0;
        BlockHeader* current = free_list[i].head;
        if(current != NULL)
        {
            cout << current->size;
        }
        while(current != NULL)
        {
            j++;
            current = current -> next;
        }

        cout <<  ":" << j << endl;
    }
}

